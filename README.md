# README #

A small idle/incremental Unity game that revolves around the Genetic Algorithm as one of its core game mechanics.

Check out the "Generic Genetic Algorith" repository for the base implementation: https://bitbucket.org/kryzarel/generic-genetic-algorithm

Genetic Algorithm in C#, YouTube tutorial series:

### Videos ###

* Part 1 - Implementation: https://www.youtube.com/watch?v=G8KJWONEeGo
* Part 2 - Examples & Fitness Function: https://www.youtube.com/watch?v=OuP-nicgS_4
* Part 3 - Adding Individuals on New Generations: https://www.youtube.com/watch?v=OsfWyHo9qY8
* Part 4 (bonus) - Serialization aka Saving: https://www.youtube.com/watch?v=8_IzdJakKzU